package io.alfredux.minicommerce.inventory.domain.model;


import io.alfredux.minicommerce.inventory.domain.command.AddStock;
import io.alfredux.minicommerce.inventory.domain.command.CreateInventory;
import io.alfredux.minicommerce.inventory.domain.event.InventoryCreated;
import io.alfredux.minicommerce.inventory.domain.event.StockAdded;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class InventoryTest {

    private FixtureConfiguration<Inventory> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(Inventory.class);
    }

    @Test
    void givenANewInventoryItIsCreatedCorrectly() {
        UUID inventoryId = UUID.randomUUID();
        fixture.givenNoPriorActivity()
                .when(CreateInventory.builder()
                        .id(inventoryId)
                        .productName("pn")
                        .productDescription("pd")
                        .productPrice(0.0)
                        .build())
                .expectEvents(InventoryCreated.builder()
                        .productName("pn")
                        .productDescription("pd")
                        .productPrice(0.0)
                        .id(inventoryId)
                        .build());
    }

    @Test
    public void givenNewStockItIsAddedCorrectly() {
        UUID inventoryId = UUID.randomUUID();
        long amount = 100L;
        fixture.given(InventoryCreated.builder()
                .id(inventoryId)
                .productName("pn")
                .productDescription("pd")
                .productPrice(0.0)
                .build())
                .when(AddStock.builder()
                        .id(inventoryId)
                        .amount(amount)
                        .build())
                .expectEvents(StockAdded.builder()
                        .id(inventoryId)
                        .amount(amount)
                        .build())
                .expectState(inventory -> assertThat(inventory.amount).isEqualTo(100L));

    }

}
