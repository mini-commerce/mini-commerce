package io.alfredux.minicommerce.inventory.api.v1.inventory;

import io.alfredux.minicommerce.inventory.domain.command.AddStock;
import io.alfredux.minicommerce.inventory.domain.command.CreateInventory;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(InventoryController.class)
class InventoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommandGateway commandGateway;

    @Test
    void givenANewInventoryCreateInventoryCommandIsSent() throws Exception {

        this.mockMvc.perform(post("/inventory/v1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Test Product\",\"description\":\"Test description\",\"price\":200}"))
                .andExpect(status().isOk());

        ArgumentCaptor<CreateInventory> captor = ArgumentCaptor.forClass(CreateInventory.class);
        verify(commandGateway).send(captor.capture());
        CreateInventory command = captor.getValue();
        log.info("command: {}", command);
    }

    @Test
    void givenANewRequestToAddStockAddStockCommandIsSent() throws Exception {

        UUID inventoryId = UUID.randomUUID();

        this.mockMvc.perform(post("/inventory/v1/" + inventoryId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\":1}"))
                .andExpect(status().isOk());

        ArgumentCaptor<AddStock> captor = ArgumentCaptor.forClass(AddStock.class);
        verify(commandGateway).send(captor.capture());
        AddStock command = captor.getValue();
        assertThat(command.getId()).isEqualTo(inventoryId);
        assertThat(command.getAmount()).isEqualTo(1);
        log.info("command: {}", command);
    }

}
