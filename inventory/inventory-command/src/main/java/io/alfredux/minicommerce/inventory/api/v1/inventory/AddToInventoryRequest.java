package io.alfredux.minicommerce.inventory.api.v1.inventory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class AddToInventoryRequest {

    private final Long amount;

    public AddToInventoryRequest(@JsonProperty("amount") @NotNull Long amount) {
        this.amount = amount;
    }
}
