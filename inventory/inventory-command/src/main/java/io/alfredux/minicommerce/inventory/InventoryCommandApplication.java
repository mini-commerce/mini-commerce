package io.alfredux.minicommerce.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventoryCommandApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryCommandApplication.class, args);
    }

}
