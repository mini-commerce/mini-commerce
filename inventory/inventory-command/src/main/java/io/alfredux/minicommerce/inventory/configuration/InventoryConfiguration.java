package io.alfredux.minicommerce.inventory.configuration;

import io.alfredux.minicommerce.common.configuration.axon.AxonConfiguration;
import io.alfredux.minicommerce.common.configuration.cors.CORSConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({AxonConfiguration.class, CORSConfig.class})
public class InventoryConfiguration {
}
