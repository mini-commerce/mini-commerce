package io.alfredux.minicommerce.inventory.api.v1.inventory;

import io.alfredux.minicommerce.inventory.domain.command.AddStock;
import io.alfredux.minicommerce.inventory.domain.command.CreateInventory;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/inventory/v1")
public class InventoryController {

    private final CommandGateway commandGateway;

    @PostMapping
    public UUID createInventory(@RequestBody CreateInventoryRequest request) {

        UUID inventoryId = UUID.randomUUID();

        commandGateway.send(CreateInventory.builder()
                .id(inventoryId)
                .productName(request.getName())
                .productDescription(request.getDescription())
                .productPrice(request.getPrice())
                .build());

        return inventoryId;
    }

    @PostMapping("/{inventoryId}")
    public void addToInventory(
            @PathVariable("inventoryId") UUID inventoryId,
            @RequestBody AddToInventoryRequest request) {
        commandGateway.send(AddStock
                .builder()
                .id(inventoryId)
                .amount(request.getAmount())
                .build());
    }
}
