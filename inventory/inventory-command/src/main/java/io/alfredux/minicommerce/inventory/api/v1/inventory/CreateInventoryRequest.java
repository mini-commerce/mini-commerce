package io.alfredux.minicommerce.inventory.api.v1.inventory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
public class CreateInventoryRequest {

    private final String name;
    private final String description;
    private final Double price;

    @Builder
    public CreateInventoryRequest(
            @JsonProperty("name") @NotEmpty String name,
            @JsonProperty("description") @NotEmpty  String description,
            @JsonProperty("price") @NotNull Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
}
