package io.alfredux.minicommerce.inventory.domain.model;

import io.alfredux.minicommerce.inventory.domain.command.AddStock;
import io.alfredux.minicommerce.inventory.domain.command.CreateInventory;
import io.alfredux.minicommerce.inventory.domain.event.InventoryCreated;
import io.alfredux.minicommerce.inventory.domain.event.StockAdded;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
public class Inventory {

    @AggregateIdentifier
    UUID id;

    @NotEmpty
    Long amount;

    @CommandHandler
    public Inventory(CreateInventory command) {
        this.id = command.getId();
        apply(InventoryCreated.builder()
                .id(id)
                .productName(command.getProductName())
                .productDescription(command.getProductDescription())
                .productPrice(command.getProductPrice())
                .build());
    }

    @EventSourcingHandler
    public void on(InventoryCreated event) {
        this.id = event.getId();
        this.amount = 0L;
    }

    @CommandHandler
    public void on(AddStock command) {
        apply(StockAdded.builder()
                .amount(command.getAmount())
                .id(command.getId())
                .build());
    }

    @EventSourcingHandler
    public void on(StockAdded event) {
        this.amount += event.getAmount();
    }

}

