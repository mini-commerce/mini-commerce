package io.alfredux.minicommerce.api.v1;


public class ProductAPIPath {

    private ProductAPIPath() {
    }

    public static final String API_BASE_URL = "/product/v1";
}
