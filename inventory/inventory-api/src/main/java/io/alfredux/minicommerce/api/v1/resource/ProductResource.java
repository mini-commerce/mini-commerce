package io.alfredux.minicommerce.api.v1.resource;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@Builder
public class ProductResource {

    UUID id;

    @NotEmpty
    String name;

    @NotEmpty
    String description;

    @NotNull
    Double price;

}
