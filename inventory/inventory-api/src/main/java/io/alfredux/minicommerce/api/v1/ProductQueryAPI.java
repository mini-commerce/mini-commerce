package io.alfredux.minicommerce.api.v1;

import io.alfredux.minicommerce.api.v1.resource.ProductResource;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ProductQueryAPI {

    @GetMapping(params = {"page", "size"})
    Page<ProductResource> listProducts(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size);
}
