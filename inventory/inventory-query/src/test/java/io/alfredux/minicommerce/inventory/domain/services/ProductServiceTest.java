package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.inventory.domain.model.Product;
import io.alfredux.minicommerce.inventory.domain.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ProductServiceTest {

    @Autowired
    ProductService productService;

    @Autowired
    ProductRepository productRepository;

    @BeforeEach
    void setUp() {
        productRepository.deleteAll();
    }

    @Test
    void givenAnEmptyRepositoryFindAllReturnsEmptyPage() {
        Page<Product> products = productService.findAll(PageRequest.of(0, 10));
        assertThat(products.getTotalElements()).isEqualTo(0);
    }

    @Test
    void givenAProductIsSavedThenItCanBeRetrieved() {
        Product savedProduct = productService.save(newTestProduct());
        assertThat(productService.findById(savedProduct.getId())).isNotEmpty();
    }

    @Test
    void givenMultipleChangesOnAProductTheseArePersisted() {
        Product savedProduct = productService.save(newTestProduct());
        Product retrievedProduct = productService.findById(savedProduct.getId())
                .orElseThrow(() -> new IllegalStateException("Product not found"));
        assertThat(retrievedProduct.getDescription()).isEqualTo(savedProduct.getDescription());
        retrievedProduct.setDescription("new description");
        Product twiceSavedProduct = productService.save(retrievedProduct);
        Product twiceRetrievedProduct = productService.findById(twiceSavedProduct.getId())
                .orElseThrow(() -> new IllegalStateException("Product not found"));
        assertThat(twiceRetrievedProduct.getDescription()).isEqualTo(twiceSavedProduct.getDescription());
    }

    @Test
    void givenAProductIsSavedThenItCanBeDeleted() {
        Product savedProduct = productService.save(newTestProduct());
        productService.deleteById(savedProduct.getId());
    }

    private Product newTestProduct() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setPrice(100d);
        product.setDescription("Product description");
        product.setName("Test Product");
        return product;
    }
}
