package io.alfredux.minicommerce.inventory.api.v1.product;

import io.alfredux.minicommerce.inventory.domain.model.Product;
import io.alfredux.minicommerce.inventory.domain.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.UUID;

import static io.alfredux.minicommerce.api.v1.ProductAPIPath.API_BASE_URL;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@Slf4j
@WebMvcTest(ProductQueryController.class)
class ProductQueryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @BeforeEach
    void beforeAll() {
        when(productService.findAll(any())).thenReturn(new PageImpl<>(Collections.singletonList(testProduct())));
    }

    private Product testProduct() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Test product");
        product.setDescription("Test description");
        product.setPrice(100.0);
        return product;
    }

    @Test
    void givenACallToTheProductListResultsAreCorrect() throws Exception {
        String url = API_BASE_URL + "/product?page=1&size=1";
        log.info("Calling url: {}", url);
        MockHttpServletResponse response = mockMvc.perform(get(url))
                .andReturn()
                .getResponse();
        log.info("List Products: {}", response.getContentAsString());
    }
}
