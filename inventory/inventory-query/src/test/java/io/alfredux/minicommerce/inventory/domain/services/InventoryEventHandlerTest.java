package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.inventory.domain.event.InventoryCreated;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class InventoryEventHandlerIntegrationTest {

    @Autowired
    private InventoryEventHandler inventoryEventHandler;

    @Autowired
    private ProductService productService;

    @Test
    void givenANewInventoryIsCreatedItIsAddedToTheProductCatalog() {

        UUID id = UUID.randomUUID();

        inventoryEventHandler.on(InventoryCreated.builder()
                .id(id)
                .productName("pn")
                .productDescription("pd")
                .productPrice(0.0)
                .build());

        assertThat(productService.findById(id)).isPresent();

    }
}
