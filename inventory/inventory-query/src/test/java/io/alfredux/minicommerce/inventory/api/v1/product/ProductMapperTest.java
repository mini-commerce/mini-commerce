package io.alfredux.minicommerce.inventory.api.v1.product;

import io.alfredux.minicommerce.api.v1.resource.ProductResource;
import io.alfredux.minicommerce.inventory.domain.model.Product;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ProductMapperTest {


    @Test
    void givenAResourceTheProductIsMappedCorrectly() {

        UUID id = UUID.randomUUID();
        ProductResource resource = ProductResource.builder()
                .id(id)
                .name("product")
                .description("test description")
                .price(10.0)
                .build();

        Product product = ProductMapper.of(resource);
        assertThat(product.getId()).isEqualTo(id);
        assertThat(product.getName()).isEqualTo("product");
        assertThat(product.getDescription()).isEqualTo("test description");
        assertThat(product.getPrice()).isEqualTo(10.0);

    }

    @Test
    void givenAProductTheResourceIsMappedCorrectly() {
        UUID id = UUID.randomUUID();
        Product product = new Product();
        product.setId(id);
        product.setName("product");
        product.setDescription("test description");
        product.setPrice(10.0);

        ProductResource resource = ProductMapper.of(product);
        assertThat(resource.getId()).isEqualTo(id);
        assertThat(resource.getName()).isEqualTo("product");
        assertThat(resource.getDescription()).isEqualTo("test description");
        assertThat(resource.getPrice()).isEqualTo(10.0);
    }
}
