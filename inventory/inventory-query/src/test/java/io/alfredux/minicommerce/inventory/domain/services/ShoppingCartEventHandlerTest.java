package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.shoppingcart.event.ShoppingCartCheckedOut;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ShoppingCartEventHandlerTest {

    private ShoppingCartEventHandler eventHandler;

    @BeforeEach
    void setUp() {
        eventHandler = new ShoppingCartEventHandler();
    }

    @Test
    void givenACheckoutEventItIsProcessedCorrectly() {
        assertThrows(UnsupportedOperationException.class, () ->
                eventHandler.on(ShoppingCartCheckedOut.builder()
                        .shoppingCartId(UUID.randomUUID())
                        .build()));
    }
}
