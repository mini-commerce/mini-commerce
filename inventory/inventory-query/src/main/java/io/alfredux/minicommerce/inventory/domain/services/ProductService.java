package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.inventory.domain.model.Product;
import io.alfredux.minicommerce.inventory.domain.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Page<Product> findAll(Pageable pageRequest) {
        return productRepository.findAll(pageRequest);
    }

    public Product save(Product product) {
        log.debug("save {} to inventory", product);
        return productRepository.save(product);
    }

    public Optional<Product> findById(UUID productId) {
        return productRepository.findById(productId);
    }

    public void deleteById(UUID productId) {
        log.debug("Delete product with id {}", productId);
        productRepository.deleteById(productId);
    }
}
