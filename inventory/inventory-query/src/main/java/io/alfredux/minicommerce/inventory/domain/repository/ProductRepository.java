package io.alfredux.minicommerce.inventory.domain.repository;

import io.alfredux.minicommerce.inventory.domain.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, UUID> {
}
