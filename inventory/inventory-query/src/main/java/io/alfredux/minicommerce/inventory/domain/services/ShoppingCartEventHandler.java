package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.shoppingcart.event.ShoppingCartCheckedOut;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartEventHandler {

    @EventHandler
    public void on(ShoppingCartCheckedOut checkedOut) {
        throw new UnsupportedOperationException();
    }
}
