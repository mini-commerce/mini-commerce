package io.alfredux.minicommerce.inventory.api.v1.product;

import io.alfredux.minicommerce.api.v1.resource.ProductResource;
import io.alfredux.minicommerce.inventory.domain.model.Product;

public class ProductMapper {

    private ProductMapper() {
    }

    public static ProductResource of(Product product) {
        return ProductResource.builder()
                .description(product.getDescription())
                .name(product.getName())
                .id(product.getId())
                .price(product.getPrice())
                .build();
    }

    public static Product of(ProductResource resource) {
        Product product = new Product();
        product.setId(resource.getId());
        product.setName(resource.getName());
        product.setDescription(resource.getDescription());
        product.setPrice(resource.getPrice());
        return product;
    }
}
