package io.alfredux.minicommerce.inventory.api.v1.product;

import io.alfredux.minicommerce.api.v1.ProductQueryAPI;
import io.alfredux.minicommerce.api.v1.resource.ProductResource;
import io.alfredux.minicommerce.inventory.domain.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static io.alfredux.minicommerce.api.v1.ProductAPIPath.API_BASE_URL;

@RestController
@RequestMapping(value = API_BASE_URL)
@RequiredArgsConstructor
public class ProductQueryController implements ProductQueryAPI {

    private final ProductService productService;

    @Override
    @GetMapping(value = "/product", params = {"page", "size"})
    public Page<ProductResource> listProducts(
            @RequestParam(required = false, value = "page", defaultValue = "0") int page,
            @RequestParam(required = false, value = "size", defaultValue = "10") int size) {

        return productService
                .findAll(PageRequest.of(page, size))
                .map(ProductMapper::of);
    }

}
