package io.alfredux.minicommerce.inventory.domain.services;

import io.alfredux.minicommerce.inventory.domain.event.InventoryCreated;
import io.alfredux.minicommerce.inventory.domain.model.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
@ProcessingGroup("inventory")
public class InventoryEventHandler {

    private final ProductService productService;

    @EventHandler
    public void on(InventoryCreated inventoryCreated) {
        log.info("Event {}", inventoryCreated);
        Product product = new Product();
        product.setId(inventoryCreated.getId());
        product.setName(inventoryCreated.getProductName());
        product.setDescription(inventoryCreated.getProductDescription());
        product.setPrice(inventoryCreated.getProductPrice());
        productService.save(product);
    }
}
