package io.alfredux.minicommerce.inventory.domain.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class Product {

    @Id
    private UUID id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @NotNull
    private Double price;
}
