package io.alfredux.minicommerce.inventory.domain.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
public class StockTaken {

    @NonNull
    UUID id;

    @NonNull
    Long amount;

    @Builder
    public StockTaken(@JsonProperty("id") UUID id,
                      @JsonProperty("amount") @NonNull Long amount) {
        this.id = id;
        this.amount = amount;
    }
}
