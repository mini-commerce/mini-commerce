package io.alfredux.minicommerce.inventory.domain.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class AddStock {

    @TargetAggregateIdentifier
    UUID id;

    @NonNull
    Long amount;

    @Builder
    public AddStock(@JsonProperty("id") UUID id,
                    @JsonProperty("amount") @NonNull Long amount) {
        this.id = id;
        this.amount = amount;
    }
}
