package io.alfredux.minicommerce.inventory.domain.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class CreateInventory {

    @TargetAggregateIdentifier
    UUID id;

    String productName;

    String productDescription;

    Double productPrice;

    @Builder
    private CreateInventory(
            @JsonProperty("id") @NonNull UUID id,
            @JsonProperty("productName") @NonNull String productName,
            @JsonProperty("productDescription") @NonNull String productDescription,
            @JsonProperty("productPrice") @NonNull Double productPrice) {
        assert !productName.isEmpty();
        assert !productDescription.isEmpty();
        this.id = id;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
    }
}
