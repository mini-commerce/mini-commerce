package io.alfredux.minicommerce.inventory.domain.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
public class InventoryCreated {

    @NonNull
    UUID id;

    String productName;

    String productDescription;

    Double productPrice;

    @Builder
    private InventoryCreated(@JsonProperty("id") @NonNull UUID id,
                             @JsonProperty("productName") @NonNull String productName,
                             @JsonProperty("productDescription") @NonNull String productDescription,
                             @JsonProperty("productPrice") @NonNull Double productPrice) {
        assert !productName.isEmpty();
        assert !productDescription.isEmpty();
        this.id = id;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
    }
}
