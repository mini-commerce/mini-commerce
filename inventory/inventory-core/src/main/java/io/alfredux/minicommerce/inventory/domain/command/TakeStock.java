package io.alfredux.minicommerce.inventory.domain.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class TakeStock {

    @TargetAggregateIdentifier
    UUID id;

    @NonNull
    String requesterId;

    @NonNull
    Long amount;

    @Builder
    public TakeStock(
            @JsonProperty("id") UUID id,
            @JsonProperty("requesterId") @NonNull String requesterId,
            @JsonProperty("amount") @NonNull Long amount) {
        this.id = id;
        this.requesterId = requesterId;
        this.amount = amount;
    }
}
