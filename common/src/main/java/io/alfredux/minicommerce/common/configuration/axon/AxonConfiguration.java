package io.alfredux.minicommerce.common.configuration.axon;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.eventhandling.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfiguration {

    @Autowired
    void configureEventBus(EventBus eventBus) {
        eventBus.registerDispatchInterceptor(new MessageLoggingDispatchInterceptor());
    }

    @Autowired
    void configureCommandBus(CommandBus commandBus) {
        commandBus.registerDispatchInterceptor(new MessageLoggingDispatchInterceptor());
    }

}
