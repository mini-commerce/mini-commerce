package io.alfredux.minicommerce.common.configuration.axon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.Message;

import java.util.List;
import java.util.function.BiFunction;

@Slf4j
public class MessageLoggingDispatchInterceptor implements org.axonframework.messaging.MessageDispatchInterceptor<Message<?>> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public BiFunction<Integer, Message<?>, Message<?>> handle(List<? extends Message<?>> messages) {
        return (index, event) -> {
            log.info("{}: {}", event.getClass().getSimpleName(), printJson(event));
            return event;
        };
    }

    private String printJson(Object data) {
        try {
            return objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            log.error("Could not serialize {}", data);
            return data.toString();
        }
    }
}
