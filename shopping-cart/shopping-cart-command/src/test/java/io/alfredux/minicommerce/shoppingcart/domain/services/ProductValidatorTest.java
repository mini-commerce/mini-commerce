package io.alfredux.minicommerce.shoppingcart.domain.services;

import io.alfredux.minicommerce.shoppingcart.value.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class ProductValidatorTest {

    private ProductValidator productValidator;

    @BeforeEach
    void setUp() {
        productValidator = new ProductValidator();
    }

    @Test
    void givenAnEmptyListReturnsFalse() {
        assertThat(productValidator.validate(Collections.emptyList())).isFalse();
    }

    @Test
    void giveAnListWithProductReturnsTrue() {
        assertThat(productValidator.validate(Collections.singletonList(Product.builder().build()))).isTrue();
    }
}
