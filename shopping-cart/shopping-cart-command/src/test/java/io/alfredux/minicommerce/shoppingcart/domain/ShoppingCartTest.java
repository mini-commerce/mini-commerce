package io.alfredux.minicommerce.shoppingcart.domain;


import io.alfredux.minicommerce.shoppingcart.command.AddProduct;
import io.alfredux.minicommerce.shoppingcart.command.Checkout;
import io.alfredux.minicommerce.shoppingcart.command.CreateShoppingCart;
import io.alfredux.minicommerce.shoppingcart.domain.error.ShoppingCartException;
import io.alfredux.minicommerce.shoppingcart.domain.services.ProductValidator;
import io.alfredux.minicommerce.shoppingcart.event.ProductAdded;
import io.alfredux.minicommerce.shoppingcart.event.ShoppingCartCreated;
import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

@Slf4j
@ExtendWith(MockitoExtension.class)
class ShoppingCartTest {

    private FixtureConfiguration<ShoppingCart> fixture;

    private final UUID testShoppingCartId = UUID.randomUUID();
    private final ShoppingCartCreated testShoppingCartCreated = ShoppingCartCreated.builder()
            .shoppingCartId(testShoppingCartId)
            .build();
    private UUID productID = UUID.randomUUID();

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(ShoppingCart.class);
        fixture.registerInjectableResource(new ProductValidator());
    }

    @Test
    public void givenANewShoppingCartItIsCreatedCorrectly() {
        fixture.givenNoPriorActivity()
                .when(createShoppingCart())
                .expectEvents(testShoppingCartCreated);
    }

    @Test
    public void givenAProductIsAddedProductAddedEventIsGenerated() {
        fixture.given(testShoppingCartCreated)
                .when(addProduct())
                .expectEvents(productAdded());
    }

    @Test
    public void givenAValidShoppingCartCheckoutWorks() {
        fixture.given(testShoppingCartCreated)
                .andGivenCommands(addProduct())
                .when(checkoutTestShoppingCart())
                .expectNoEvents();
    }

    @Test
    public void givenAnEmptyShoppingCartCantCheckout() {
        fixture.given(testShoppingCartCreated)
                .when(checkoutTestShoppingCart())
                .expectException(ShoppingCartException.class);
    }

    private CreateShoppingCart createShoppingCart() {
        return CreateShoppingCart.builder()
                .id(testShoppingCartId)
                .build();
    }

    private ProductAdded productAdded() {
        return ProductAdded.builder()
                .shoppingCartId(testShoppingCartId)
                .product(productToAdd())
                .build();
    }

    private AddProduct addProduct() {
        return AddProduct.builder()
                .shoppingCartId(testShoppingCartId)
                .product(productToAdd())
                .build();
    }

    private Product productToAdd() {
        return Product.builder()
                .id(productID)
                .amount(2L)
                .build();
    }

    private Checkout checkoutTestShoppingCart() {
        return Checkout.builder()
                .shoppingCartId(testShoppingCartId)
                .build();
    }
}
