package io.alfredux.minicommerce.shoppingcart.application.v1;

import io.alfredux.minicommerce.shoppingcart.value.Product;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ProductResourceTest {

    private final Product testProduct = Product.builder()
            .id(UUID.randomUUID())
            .amount(0L)
            .build();

    @Test
    void givenAProductTheResourceIsCreatedCorrectly() {
        ProductResource productResource1 = ProductResource.of(testProduct);
        ProductResource productResource2 = ProductResource.of(testProduct);
        assertThat(productResource1).isEqualTo(productResource2);
    }

    @Test
    void givenAResourceTheProductIsCreatedCorrectly() {
        ProductResource productResource = ProductResource.of(testProduct);
        assertThat(productResource.toProduct()).isEqualTo(testProduct);
    }
}
