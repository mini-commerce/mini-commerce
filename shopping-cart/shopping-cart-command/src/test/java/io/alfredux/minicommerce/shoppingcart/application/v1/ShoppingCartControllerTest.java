package io.alfredux.minicommerce.shoppingcart.application.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.alfredux.minicommerce.shoppingcart.command.AddProduct;
import io.alfredux.minicommerce.shoppingcart.command.Checkout;
import io.alfredux.minicommerce.shoppingcart.command.CreateShoppingCart;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(ShoppingCartController.class)
class ShoppingCartControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommandGateway commandGateway;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void givenANewShoppingCartCommandAndResponseAreCorrect() throws Exception {

        String responseBody = this.mockMvc.perform(post("/shopping-cart/v1"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ShoppingCartCommandResponse response = mapper.readValue(responseBody, ShoppingCartCommandResponse.class);
        ArgumentCaptor<CreateShoppingCart> captor = ArgumentCaptor.forClass(CreateShoppingCart.class);
        verify(commandGateway).send(captor.capture());
        CreateShoppingCart createShoppingCartCommand = captor.getValue();
        assertThat(createShoppingCartCommand.getId()).isEqualTo(response.getId());
    }

    @Test
    void givenAProductIsAddedCommandAndResponseAreCorrect() throws Exception {

        String responseBody = this.mockMvc.perform(post("/shopping-cart/v1/" + randomUUID() + "/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"" + randomUUID() + "\",\"amount\":1}"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ShoppingCartCommandResponse response = mapper.readValue(responseBody, ShoppingCartCommandResponse.class);
        ArgumentCaptor<AddProduct> captor = ArgumentCaptor.forClass(AddProduct.class);
        verify(commandGateway).send(captor.capture());
        AddProduct addProductCommand = captor.getValue();
        assertThat(addProductCommand.getShoppingCartId()).isEqualTo(response.getId());

    }

    @Test
    void givenACheckoutCommandAndResponseAreCorrect() throws Exception {

        String responseBody = this.mockMvc.perform(post("/shopping-cart/v1/" + randomUUID() + "/checkout"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ShoppingCartCommandResponse response = mapper.readValue(responseBody, ShoppingCartCommandResponse.class);
        ArgumentCaptor<Checkout> captor = ArgumentCaptor.forClass(Checkout.class);
        verify(commandGateway).send(captor.capture());
        Checkout checkoutCommand = captor.getValue();
        assertThat(checkoutCommand.getShoppingCartId()).isEqualTo(response.getId());

    }
}
