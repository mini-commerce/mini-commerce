package io.alfredux.minicommerce.shoppingcart.domain.error;

public class ShoppingCartException extends RuntimeException {

    public ShoppingCartException(String s) {
        super(s);
    }
}
