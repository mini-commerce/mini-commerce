package io.alfredux.minicommerce.shoppingcart.domain;

import io.alfredux.minicommerce.shoppingcart.command.AddProduct;
import io.alfredux.minicommerce.shoppingcart.command.Checkout;
import io.alfredux.minicommerce.shoppingcart.command.CreateShoppingCart;
import io.alfredux.minicommerce.shoppingcart.domain.error.ShoppingCartException;
import io.alfredux.minicommerce.shoppingcart.domain.services.ProductValidator;
import io.alfredux.minicommerce.shoppingcart.event.ProductAdded;
import io.alfredux.minicommerce.shoppingcart.event.ShoppingCartCreated;
import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
@Slf4j
public class ShoppingCart {

    @AggregateIdentifier
    private UUID id;

    private List<Product> products = new ArrayList<>();

    @CommandHandler
    public ShoppingCart(CreateShoppingCart createShoppingCart) {
        this.id = createShoppingCart.getId();
        apply(ShoppingCartCreated.builder()
                .shoppingCartId(id)
                .build());
    }

    @CommandHandler
    public void on(AddProduct addProduct) {
        apply(ProductAdded.builder()
                .shoppingCartId(this.id)
                .product(addProduct.getProduct())
                .build());
    }

    @CommandHandler
    public void on(Checkout checkout, @Autowired ProductValidator productValidator) {
        if (!productValidator.validate(this.products)) {
            throw new ShoppingCartException("Not valid shopping cart");
        }
    }

    @EventSourcingHandler
    public void on(ProductAdded productAdded) {
        this.products.add(productAdded.getProduct());
    }

    @EventSourcingHandler
    public void on(ShoppingCartCreated event) {
        this.id = event.getShoppingCartId();
    }
}


