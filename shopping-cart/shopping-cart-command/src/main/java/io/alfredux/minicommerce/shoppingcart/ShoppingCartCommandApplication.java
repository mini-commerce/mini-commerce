package io.alfredux.minicommerce.shoppingcart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingCartCommandApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartCommandApplication.class, args);
    }

}
