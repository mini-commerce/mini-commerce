package io.alfredux.minicommerce.shoppingcart.application.v1;

import io.alfredux.minicommerce.shoppingcart.command.AddProduct;
import io.alfredux.minicommerce.shoppingcart.command.Checkout;
import io.alfredux.minicommerce.shoppingcart.command.CreateShoppingCart;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/shopping-cart/v1")
@Slf4j
public class ShoppingCartController {

    private final CommandGateway commandGateway;

    @PostMapping
    public ShoppingCartCommandResponse createShoppingCart() {
        UUID shoppingCartUUID = UUID.randomUUID();

        commandGateway.send(CreateShoppingCart.builder()
                .id(shoppingCartUUID)
                .build());

        return ShoppingCartCommandResponse.of(shoppingCartUUID);
    }

    @PostMapping("/{shoppingCartId}/product")
    public ShoppingCartCommandResponse addProductToShoppingCart(
            @PathVariable("shoppingCartId") UUID shoppingCartId,
            @RequestBody ProductResource productResource) {

        log.info("Received shoppingCartId: {}", shoppingCartId);

        AddProduct command = AddProduct.builder()
                .shoppingCartId(shoppingCartId)
                .product(productResource.toProduct())
                .build();

        commandGateway.send(command);

        return ShoppingCartCommandResponse.of(shoppingCartId);
    }

    @PostMapping("/{shoppingCartId}/checkout")
    public ShoppingCartCommandResponse checkout(@PathVariable("shoppingCartId") UUID shoppingCartId) {

        commandGateway.send(Checkout.builder()
                .shoppingCartId(shoppingCartId)
                .build());

        return ShoppingCartCommandResponse.of(shoppingCartId);
    }


}
