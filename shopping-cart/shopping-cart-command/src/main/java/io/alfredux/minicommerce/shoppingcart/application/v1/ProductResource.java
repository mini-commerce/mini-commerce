package io.alfredux.minicommerce.shoppingcart.application.v1;

import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class ProductResource {

    UUID id;
    Long amount;

    public static ProductResource of(Product product) {
        return ProductResource.builder()
                .id(product.getId())
                .amount(product.getAmount())
                .build();
    }

    public Product toProduct() {
        return Product.builder()
                .id(id)
                .amount(amount)
                .build();
    }
}
