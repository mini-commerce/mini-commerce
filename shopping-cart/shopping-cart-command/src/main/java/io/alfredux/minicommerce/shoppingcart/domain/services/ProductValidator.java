package io.alfredux.minicommerce.shoppingcart.domain.services;

import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Service
@Slf4j
public class ProductValidator {

    public boolean validate(List<Product> products) {
        log.info("validating {}", products);
        return !CollectionUtils.isEmpty(products);

    }
}
