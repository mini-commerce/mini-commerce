package io.alfredux.minicommerce.shoppingcart.application.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
public class ShoppingCartCommandResponse {

    UUID id;
    Long time;

    @Builder
    public ShoppingCartCommandResponse(@JsonProperty("id") UUID id,
                                       @JsonProperty("time") Long time) {
        this.id = id;
        this.time = time;
    }

    public static ShoppingCartCommandResponse of(UUID id) {
        return ShoppingCartCommandResponse.builder()
                .id(id)
                .time(System.currentTimeMillis())
                .build();
    }
}
