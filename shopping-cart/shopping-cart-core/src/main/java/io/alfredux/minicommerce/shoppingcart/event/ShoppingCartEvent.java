package io.alfredux.minicommerce.shoppingcart.event;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

import java.util.UUID;

@Getter
@EqualsAndHashCode
public abstract class ShoppingCartEvent {

    private final UUID shoppingCartId;

    public ShoppingCartEvent(@NonNull UUID shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
    }

}
