package io.alfredux.minicommerce.shoppingcart.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = true)
public class ShoppingCartCreated extends ShoppingCartEvent {

    @Builder
    public ShoppingCartCreated(@JsonProperty("shoppingCartId") UUID shoppingCartId) {
        super(shoppingCartId);
    }
}
