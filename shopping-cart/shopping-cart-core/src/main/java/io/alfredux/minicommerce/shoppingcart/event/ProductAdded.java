package io.alfredux.minicommerce.shoppingcart.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = true)
public class ProductAdded extends ShoppingCartEvent {

    Product product;

    @Builder
    public ProductAdded(@JsonProperty("shoppingCartId") UUID shoppingCartId,
                        @JsonProperty("product") Product product) {
        super(shoppingCartId);
        this.product = product;
    }

}


