package io.alfredux.minicommerce.shoppingcart.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.UUID;

public class ShoppingCartCheckedOut extends ShoppingCartEvent {

    @Builder
    public ShoppingCartCheckedOut(@JsonProperty("shoppingCartId") UUID shoppingCartId) {
        super(shoppingCartId);
    }
}
