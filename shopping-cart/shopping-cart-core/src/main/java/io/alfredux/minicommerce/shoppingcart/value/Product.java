package io.alfredux.minicommerce.shoppingcart.value;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
public class Product {

    UUID id;
    Long amount;

    @Builder
    public Product(@JsonProperty("id") UUID id, @JsonProperty("amount") Long amount) {
        this.id = id;
        this.amount = amount;
    }
}
