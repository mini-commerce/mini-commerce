package io.alfredux.minicommerce.shoppingcart.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class CreateShoppingCart {

    @TargetAggregateIdentifier
    UUID id;

    @Builder
    public CreateShoppingCart(@JsonProperty("shoppingCartId") UUID id) {
        this.id = id;
    }
}
