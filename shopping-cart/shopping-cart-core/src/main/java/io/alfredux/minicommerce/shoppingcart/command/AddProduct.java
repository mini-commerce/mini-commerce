package io.alfredux.minicommerce.shoppingcart.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.alfredux.minicommerce.shoppingcart.value.Product;
import lombok.Builder;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class AddProduct {

    @TargetAggregateIdentifier
    UUID shoppingCartId;

    Product product;

    @Builder
    public AddProduct(@JsonProperty("shoppingCartId") UUID shoppingCartId,
                      @JsonProperty("product") Product product) {
        this.shoppingCartId = shoppingCartId;
        this.product = product;
    }
}
