# Mini Commerce

This is a playground project to explore how to use Axon Framework in a microservices architecture.

# System requirements

- JDK 8
- Maven 3.6
- Docker 
- Docker Compose

# How to test locally

`./mvn clean verify`

# How to run locally

## Compile the project

The command `./mvn clean install` will compile the project, create the docker images and install 
them in your local docker registry.

## Run the project

Having previously built the project, from the root folder of the repository you can run:

`docker-compose up`

It will start a docker stack with all the services.